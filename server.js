//version inicial jose luis

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3002;

var path = require('path');

app.listen(port);

//*----------------------------------------------------------------------------
//* Configuramos parte de Seguridad para enviar las peticiones a la BD (MongoDB)
var bodyParser = require( 'body-parser' );
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({ extended: true }));
app.use( function(req, res, next){
  res.header( "Access-Control-Allow-Origin", "*" );
  res.header( "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept" );
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});


// Agregamos librería request-json para atacar MLab (MongoDB)
var requestjson = require( "request-json" );
var urlDBMovimientos = "https://api.mlab.com/api/1/databases/bdbanca3m911320/collections/movimientos?apiKey=rfKYRuE0XSDDRCdTQSLQx3ILk6rsbEki";
var urlDBCuentas = "https://api.mlab.com/api/1/databases/bdbanca3m911320/collections/cuentasmovs?apiKey=rfKYRuE0XSDDRCdTQSLQx3ILk6rsbEki";
var clienteMLab = requestjson.createClient( urlDBMovimientos );
var clienteMLabCuentas = requestjson.createClient(urlDBCuentas);
// https://api.mlab.com/api/1/databases/my-db/collections/my-coll?apiKey=myAPIKey
var urlDBUsers = "https://api.mlab.com/api/1/databases/bdbanca3m54178/collections/usuarios?apiKey=yJAszOqg4qpGLsnE6JfBaHB2rPsgxRK1";
var runCommand = "https://api.mlab.com/api/1/databases/bdbanca3m54178/runCommand?apiKey=yJAszOqg4qpGLsnE6JfBaHB2rPsgxRK1"
var mlabUsuarios = requestjson.createClient( urlDBUsers );
var urlBDCuentas = "https://api.mlab.com/api/1/databases/bdbanca3m54178/collections/cuentasMov?apiKey=yJAszOqg4qpGLsnE6JfBaHB2rPsgxRK1";
var urlBDCatalogoOtrosBancos = "https://api.mlab.com/api/1/databases/bdbanca3m54178/collections/catalogoTransferenciasOtrosBancos?apiKey=yJAszOqg4qpGLsnE6JfBaHB2rPsgxRK1";
var urlBDCatalogoCuentasBBVA = "https://api.mlab.com/api/1/databases/bdbanca3m54178/collections/catalogoTransferenciasCuentasBancomer?apiKey=yJAszOqg4qpGLsnE6JfBaHB2rPsgxRK1";
var urlBDCatalogoPagoServicios = "https://api.mlab.com/api/1/databases/bdbanca3m54178/collections/catalogoPagoServicios?apiKey=yJAszOqg4qpGLsnE6JfBaHB2rPsgxRK1";
var urlBDConvenios = "https://api.mlab.com/api/1/databases/bdbanca3m54178/collections/conveniosEmpresas?apiKey=yJAszOqg4qpGLsnE6JfBaHB2rPsgxRK1";
var urlBDEmpresas = "https://api.mlab.com/api/1/databases/bdbanca3m54178/collections/empresas?apiKey=yJAszOqg4qpGLsnE6JfBaHB2rPsgxRK1";
var apiExternaNorthwind = "https://services.odata.org/Experimental/Northwind/Northwind.svc/Suppliers";
//*--------------------------------------------------------------------------

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function( req, res ){

  //* Respondemos con un texto
  //res.send('Hola mundo node');

  //* Respondemos con un fichero HTML
  //res.sendFile( path.join(__dirname, 'index.html' ) );

  //* Respondemos con un json
  res.json( "{ [  {id:1, name:'nombre1'},    {id:2, nombre:'nombre2'},     {id:3, nombre:'nombre3'}   ] }" );

});

//*
//* idcliente es el nombre de la variable donde voy a cachar el ID del cliente en la llamada GET
app.get( '/clientes/:idcliente', function(req, res){

    res.send('Aquí tiene el cliente número ' + req.params.idcliente );
});

app.get('/movimientos/:idCliente', function(req, res){
  clienteMLab.get( '', function( err, resM, body ){
    if( err ){
      console.log( err );
    }else{
      // res.send( body );
      res.send('Aquí tiene el cliente número ' + req.params );
    }
  });
});

app.get('/cuentas/:idCliente', function(req, res){
  var horaActual = new Date();
  console.log(horaActual + " -- obteniendo cuentas por cliente");
  var idCliente = req.params.idCliente;
  clienteMLabCuentas.get( '', function( err, resM, body ){
    if( err ){
      console.log( err );
    }else{
      // res.send( body );
      var busquedaCliente = body.filter(function(item) {
        return item.cliente == idCliente;
      });
      res.send(busquedaCliente[0].movimientos);
      }
    });
  });

app.post( '/loginUser', function( req, res ){
  var horaActual = new Date();
  console.log(horaActual + " -- Verificando cuenta de usuario");
  var password = req.body.password;
  var email = req.body.email;
  var query = urlDBUsers + '&q={"Email":"' + email + '"}&l=1';
  console.log(query);
  var mlabApi = requestjson.createClient(query);

  mlabApi.get('', (err, resM, body) => {
    var objUserResult = {};
    if(err){
      console.error(err);
    }else {
      var objUser = {};
      objUser = body[0];
      if(objUser != undefined){
        if(objUser.Password == password){
          objUserResult = {
            idCliente : objUser.IdCliente,
            nombre : objUser.Nombre,
            apellido : objUser.Apellido,
            email : objUser.Email
          }
        }
      }
    }
    res.send( objUserResult );
  });
});

function consultarCuentaPorEmail(email){

    var horaActual = new Date();
    console.log(horaActual + " -- Consultando cuenta por Email");
    var user = {};
    var query = urlDBUsers + '&q={"Email":"' + email + '"}&l=1'
    console.log(query);
    var mlabApi = requestjson.createClient(query);

    mlabApi.get('', (err, resM, body) => {
      if(err){
        console.error(err);
        reject(err);
      }else {
        console.log("mostrando usuario resulto por promise " + objUser.Nombre);
        resolve(JSON.parse(body));
      }
    });
}

app.post( '/registroUsuario', function( req, res, next ){
  console.log(new Date() + " -- Registrando nuevo usuario");

  var promiseRegistro = new Promise(function (resolve, reject){
    mlabUsuarios.post('', req.body, function(err, res, body) {
      if(res.statusCode == 200){
        resolve(res.statusCode);
      }else {
        reject(err);
      }
      // return console.log(res.statusCode);
    });
  });

  promiseRegistro.then((statusCode)=>{
    next();
  });
}, function (req, res, next) {
    console.log(new Date() + " -- Registrando id Para catalogo de transferencias otros bancos");
    var objCatalogo = {
      idCliente : req.body.IdCliente,
      catalogoDeCuentas: []
    };
    var mlabApi = requestjson.createClient(urlBDCatalogoOtrosBancos);
    mlabApi.post('', objCatalogo, function(err, res, body){
      if(res.statusCode == 200)
        return console.log(res.statusCode);
    });

    res.send("se ha creado usuario");
  }
  // res.send( 'Se ha creado usuario' );
);

app.get('/v2/cuentas/:numCliente', function(req, res){
  var horaActual = new Date();
  console.log(horaActual + " -- obteniendo cuentas por numero de cliente v2");
  var numCliente = req.params.numCliente;
  var queryCuentas = urlBDCuentas + '&q={"cliente":' + numCliente + "}";
  var mlabUsuarios = requestjson.createClient( queryCuentas );
  mlabUsuarios.get('', function(err, resM, body){
    if(err){
      console.log(err);
    }else{

      var arrayCuentas = [];
      var i = 0;

      while(body[i] != undefined){
        arrayCuentas[i] = body[i].idCuenta;
        i++;
      }
      res.send(arrayCuentas);
    }
  });
});


app.get('/v2/cuentas/importe/total', (req, res) => {
  var horaActual = new Date();
  console.log("\n" + horaActual + " -- calculando importe v2");
  var query = urlBDCuentas + '&q={"cliente":' + req.query.cliente + ',"idCuenta":' + req.query.cuenta + '}'
  var mlabApi = requestjson.createClient(query);
  mlabApi.get('', (err, resM, body) => {
    if(err){
      console.error(err);
    }else {
      var objTotal;

      if(body[0] != undefined){
        var listMovimientos = body[0].listadoMovimientos;
        var i = 0;
        var importeTotal = 0;

        while(listMovimientos[i] != undefined){
          importeTotal += parseFloat(listMovimientos[i].importe);
          i++;
        }

        objTotal = {
          total : importeTotal
        };
      }else{
        objTotal = {
          total : 0
        }
      }
      console.log(objTotal);
      res.send(objTotal);
    }
  });
});

app.get('/v2/movimientos', (req, res) => {
  var horaActual = new Date();
  console.log("\n" + horaActual + " -- buscando movimientos");
  var query = urlBDCuentas + '&q={"cliente":' + req.query.cliente + ',"idCuenta":' + req.query.cuenta + '}';
  console.log(query);
  var mlabApi = requestjson.createClient(query);
  mlabApi.get('', (err, resM, body) => {
    if(err){
      console.error(err);
    }else {
      var listMovimientos = [];
      if (body[0] != undefined){
        listMovimientos = body[0].listadoMovimientos;
      }
      res.send(listMovimientos);
    }
  });
});

app.get('/v2/idCliente', (req, res) => {
  var horaActual = new Date()
  console.log("\n" + horaActual + " -- checando ultimo id usuario");
  var query = urlDBUsers + '&s={"IdCliente":-1}&f={"IdCliente":1}&l=1'
  var mlabApi = requestjson.createClient(query);
  mlabApi.get('', (err, resM, body) => {
    if(err){
      console.error(err);
    }else {
      var idCliente = [];

      if (body[0] != undefined){
        idCliente.push("idCliente", body[0].IdCliente);
      }
      res.send(idCliente);
    }
  });
});

app.post( '/v2/update/auto/incremento/movimientos', function( req, res, next ){
  var horaActual = new Date();
  var query = runCommand;
  var mlabApi = requestjson.createClient(query);
  var idActualizado;
  console.log(horaActual + " -- incrementando id de movimientos");

  //objecto creado para correrlo con el comando especial de mongo
  var objFindAndModify = {
    findAndModify : "incrementoIdMovimientos",
    query : req.body.objUpdate,
    update: {
      $inc: {
        idMovimiento:1
      }
    },
    new: true
  }

  var promiseModify = new Promise(function (resolve, reject){
    mlabApi.post('', objFindAndModify, (err, res, body) => {
        console.log("-- Estatus del codigo al incrementar id " + res.statusCode);
        if(body){
          resolve(body.value.idMovimiento);
        }else {
          reject(err)
        }
    });
  });

  promiseModify.then((idNuevo)=>{
    this.idActualizado = idNuevo;
    next();
  });

  }, function (req, res, next) {
    horaActual = new Date();
    console.log(horaActual + " -- Generando registro de pago");

    var query2 = urlBDCuentas + '&q={"cliente":' + req.body.objUpdate.cliente + ',"idCuenta":' + req.body.objUpdate.idCuenta + '}';
    var mlabApiAddMov = requestjson.createClient( query2 );
    var objAdd = req.body.listaMovimientos;
    objAdd.id = idActualizado;

    var objNuevo = {
      $push: {
        listadoMovimientos: objAdd
      }
    }

    mlabApiAddMov.put('', objNuevo, (errP, resP, bodyP) => {
      console.log(resP.statusCode);
      var objFinalRespuesta = {
        statusCode : resP.statusCode
      }
      res.send( objFinalRespuesta );
    });

  }
);

app.get('/catalogo/transferencias/otrosBancos', (req, res, next) => {
  console.log("\n" + new Date() + " -- obteniendo catalogo de transferencias de otros bancos");
  var query = urlBDCatalogoOtrosBancos + '&q={"idCliente":' + req.query.cliente + '}';
  console.log(query);
  var mlabApi = requestjson.createClient(query);
  mlabApi.get('', (err, resM, body) => {

    if(err){
      console.error(err);
    }else {
      res.send(body);
    }
  });
});

app.get('/catalogo/transferencias/cuentas/bancomer', (req, res, next) => {
  console.log("\n" + new Date() + " -- obteniendo catalogo de transferencias de cuentas bancomer");
  var query = urlBDCatalogoCuentasBBVA + '&q={"idCliente":' + req.query.cliente + '}';
  console.log(query);
  var mlabApi = requestjson.createClient(query);
  mlabApi.get('', (err, resM, body) => {

    if(err){
      console.error(err);
    }else {
      res.send(body);
    }
  });
});

app.get('/catalogo/pago/servicios', (req, res, next) => {
  console.log("\n" + new Date() + " -- obteniendo catalogo de pago de servicios");
  var query = urlBDCatalogoPagoServicios + '&q={"idCliente":' + req.query.cliente + '}';
  var mlabApi = requestjson.createClient(query);
  mlabApi.get('', (err, resM, body) => {
    if(err){
      console.error(err);
    }else {
      res.send(body);
    }
  });
});

app.get('/get/convenios', (req, res, next) => {
  console.log("\n" + new Date() + " -- obteniendo convenios");
  var query = urlBDConvenios;
  var mlabApi = requestjson.createClient(query);
  mlabApi.get('', (err, resM, body) => {
    if(err){
      console.error(err);
    }else {
      res.send(body);
    }
  });
});

app.post( '/guarda/cuenta/otros/bancos', function( req, res, next ){
  console.log(new Date() + " -- guardando registro de cuenta de otros bancos");

  var query = urlBDCatalogoOtrosBancos + '&q={"idCliente":' + req.body.datosAcceso.idCliente + '}';
  console.log(query);

  var mlabApi = requestjson.createClient( query );

  var objNuevo = req.body.objCuenta;

  mlabApi.post('', objNuevo, (errP, resP, bodyP) => {
    var objFinalRespuesta = {
      statusCode : resP.statusCode
    }
    res.send( objFinalRespuesta );
  });

});

app.post( '/guarda/cuentas/bancomer', function( req, res, next ){
  console.log(new Date() + " -- guardando registro de cuentas bancomer");

  var query = urlBDCatalogoCuentasBBVA + '&q={"idCliente":' + req.body.datosAcceso.idCliente + '}';
  console.log(query);

  var mlabApi = requestjson.createClient( query );

  var objNuevo = req.body.objCuenta;

  mlabApi.post('', objNuevo, (errP, resP, bodyP) => {
    var objFinalRespuesta = {
      statusCode : resP.statusCode
    }
    res.send( objFinalRespuesta );
  });

});


app.post( '/actualizar/cuenta/otros/bancos', function( req, res, next ){
  console.log(new Date() + " -- guardando registro de cuenta de otros bancos");
  var numeroCuenta = '"' +req.body.datosAcceso.numeroCuenta + '"';
  var query = urlBDCatalogoOtrosBancos + '&q={"idCliente":' + req.body.datosAcceso.idCliente + ',"numTarjetaCuenta":' + numeroCuenta +'}';
  console.log(query);
  var mlabApi = requestjson.createClient( query );
  var objNuevo = {
    $set: {
      nombreCorto: req.body.update.nombreCorto
    }
  }

  mlabApi.put('', objNuevo, (errP, resP, bodyP) => {
    if(errP){
      console.error(errP);
    }else{
      var objFinalRespuesta = {
        statusCode : resP.statusCode
      }
      res.send( objFinalRespuesta );
    }

  });

});

app.post( '/actualizar/cuentas/bancomer', function( req, res, next ){
  console.log(new Date() + " -- Actualizando registro cuentas bancomer");
  var numeroCuenta = '"' +req.body.datosAcceso.numeroCuenta + '"';
  var query = urlBDCatalogoCuentasBBVA + '&q={"idCliente":' + req.body.datosAcceso.idCliente + ',"numTarjetaCuenta":' + numeroCuenta +'}';
  console.log(query);
  var mlabApi = requestjson.createClient( query );
  var objNuevo = {
    $set: {
      nombreCorto: req.body.update.nombreCorto
    }
  }

  mlabApi.put('', objNuevo, (errP, resP, bodyP) => {
    if(errP){
      console.error(errP);
    }else{
      var objFinalRespuesta = {
        statusCode : resP.statusCode
      }
      res.send( objFinalRespuesta );
    }

  });

});

app.post( '/inserta/movimiento', function( req, res, next ){
  var query = runCommand;
  var mlabApi = requestjson.createClient(query);
  var idActualizado;
  console.log(new Date() + " -- incrementando id de movimientos");

  //objecto creado para correrlo con el comando especial de mongo
  var objFindAndModify = {
    findAndModify : "incrementoIdMovimientos",
    query : req.body.objUpdate,
    update: {
      $inc: {
        idMovimiento:1
      }
    },
    new: true
  }

  var promiseModify = new Promise(function (resolve, reject){
    mlabApi.post('', objFindAndModify, (err, res, body) => {
        console.log("-- Estatus del codigo al incrementar id " + res.statusCode);
        if(body){
          resolve(body.value.idMovimiento);
        }else {
          reject(err)
        }
    });
  });

  promiseModify.then((idNuevo)=>{
    this.idActualizado = idNuevo;
    next();
  });

  }, function (req, res, next) {
    horaActual = new Date();
    console.log(horaActual + " -- Generando registro de pago");

    var query2 = urlBDCuentas + '&q={"cliente":' + req.body.objUpdate.cliente + ',"idCuenta":' + req.body.objUpdate.idCuenta + '}';
    var mlabApiAddMov = requestjson.createClient( query2 );
    var objAdd = req.body.listaMovimientos;
    objAdd.id = idActualizado;

    var objNuevo = {
      $push: {
        listadoMovimientos: objAdd
      }
    }

    mlabApiAddMov.put('', objNuevo, (errP, resP, bodyP) => {
      var objFinalRespuesta = {
        statusCode : resP.statusCode,
        folioInternet: objAdd.id
      }
      res.send( objFinalRespuesta );
    });

  }
);

app.get('/get/empresa', (req, res, next) => {
  console.log("\n" + new Date() + " -- obteniendo empresa");
  var query = urlBDEmpresas + '&q={"numConvenio":' + req.query.numConvenio + '}';
  var mlabApi = requestjson.createClient(query);
  mlabApi.get('', (err, resM, body) => {
    if(err){
      console.error(err);
    }else {
      res.send(body);
    }
  });
});

app.get('/get/empresa/api', (req, res, next) => {
  console.log("\n" + new Date() + " -- obteniendo empresa de api ");
  var query = apiExternaNorthwind + '?$filter=SupplierID eq ' + req.query.supplierID;
  var mlabApi = requestjson.createClient(query);
  mlabApi.get('', (err, resM, body) => {
    if(err){
      console.error(err);
    }else {

      var jsonRespuesta = [{
        "numConvenio": req.query.numConvenio,
        "razonSocial": body.value[0].CompanyName,
        "coberturaOCiudad": body.value[0].City,
        "tipoPago": "Mensual",
        "nombreComercial": body.value[0].ContactName,
        "etiquetaReferencia": "Referencia"
      }];
      res.send(jsonRespuesta);
    }
  });
});

app.post( '/guardar/pago/servicio', function( req, res, next ){
  console.log(new Date() + " -- guardando pago de servicios");

  var query = urlBDCatalogoPagoServicios;
  var mlabApi = requestjson.createClient( query );
  var objNuevo = req.body.objEmpresa;

  mlabApi.post('', objNuevo, (errP, resP, bodyP) => {
    var objFinalRespuesta = {
      statusCode : resP.statusCode
    }
    res.send( objFinalRespuesta );
  });

});

app.post( '/actualizar/catalogo/pago/servicios', function( req, res, next ){
  console.log(new Date() + " -- actualizando catalogo de pago de servicios");
  var referencia = '"' +req.body.datosAcceso.referencia + '"';
  var query = urlBDCatalogoPagoServicios + '&q={"idCliente":' + req.body.datosAcceso.idCliente + ',"referencia":' + referencia +'}';
  console.log(query);
  var mlabApi = requestjson.createClient( query );
  var objNuevo = {
    $set: {
      nombreCorto: req.body.update.nombreCorto
    }
  }

  mlabApi.put('', objNuevo, (errP, resP, bodyP) => {
    if(errP){
      console.error(errP);
    }else{
      var objFinalRespuesta = {
        statusCode : resP.statusCode
      }
      res.send( objFinalRespuesta );
    }

  });

});
/*
app.post('/elimiar/registro/catalogo', fuction(req, res, next){
  console.log(new Date() + " -- eliminar catalogo de pago de transferencias");
});
*/

app.put( '/v2/update/avimientos', function( req, res ){
  var horaActual = new Date();
  console.log(horaActual + " -- Verificando cuenta de usuario");
  res.send( 'Su petición PUT ha sido recibida' );
});

app.delete( '/', function( req, res ){
  res.send( 'Su petición DELETE ha sido recibida' );
});
